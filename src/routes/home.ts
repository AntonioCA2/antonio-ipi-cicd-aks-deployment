import express from 'express';

const router = express.Router();

router.get('/', (req, res) => {
  res.send('Hello, World! I am a Node.js server.');
  //res.send('Hello, John Doe!'); // <-- message modifié
});

export default router;

